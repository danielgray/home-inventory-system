//Setup environment variables
require("dotenv").config();
//Require modules
const express = require("express"),
  MongoClient = require("mongodb").MongoClient,
  bodyParser = require("body-parser"),
  app = express(),
  fs = require("fs"),
  https = require("https"),
  sentry = require("@sentry/node");

//Backend error logging to sentry
sentry.init({
  dsn: process.env.sentryURL,
  environment: process.env.environment,
});

//Setup HTTPS
const privateKey = fs.readFileSync("ssl/key.key", "utf8");
const certificate = fs.readFileSync("ssl/cert.pem", "utf8");
const credentials = { key: privateKey, cert: certificate };
const httpsServer = https.createServer(credentials, app);

//Configure and start webserver
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.disable("x-powered-by");
httpsServer.listen(process.env.port);
console.info(`home-inventory-system running on port ${process.env.port}`);

//React frontend
app.use(express.static(`${__dirname}/build`));
app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});
app.get("/search", function (req, res) {
  res.redirect("/");
});
app.get("/add", function (req, res) {
  res.redirect("/");
});

//API routing
app.get("/api/search", async (req, res, next) => {
  //Connect
  MongoClient.connect(process.env.mongourl, function (err, client) {
    if (err) res.status(500).json({ error: err });
    //Select collection
    let collection = client.db("home-inventory-system").collection("items");

    //Manipulate string
    let query;
    let item = decodeURIComponent(req.query.itemtofind).toLowerCase();
    query = { item: item };
    //Search
    collection.find(query).toArray(function (err, result) {
      if (err) res.status(500).json({ error: err });
      //If nothing found
      if (!result.length) {
        client.close();
        res.json({ error: "item not found" });
      } else {
        let results = [];

        //Setup promise so that it doesn't suddenly send back not all results
        new Promise((resolve, reject) => {
          //Loop for multiple results
          let count = 0;
          for (i = 0; i < result.length; i++) {
            if (!result[i].box.inBox) {
              //Not in a box
              results.push({ location: result[i].location });
              if (count == i) {
                resolve();
              }
            } else {
              //change collection to get box details
              collection = client
                .db("home-inventory-system")
                .collection("boxes");
              collection.findOne({ boxid: result[i].box.boxid }, function (
                err,
                boxresult
              ) {
                //Push results to array
                results.push({
                  location: boxresult.shelf,
                  box: boxresult.name,
                });
                if (count == i) {
                  resolve();
                }
              });
            }
            count = count + 1;
          }
        }).then(() => {
          //Close the connection
          client.close();
          //Send the results
          res.json({ results: results });
        });
      }
    });
  });
});

app.get("/api/getcontents", async (req, res, next) => {
  //Connect to database
  MongoClient.connect(process.env.mongourl, function (err, client) {
    if (err) res.status(500).json({ error: err });
    //Select collection
    let collection = client.db("home-inventory-system").collection("boxes");
    let query = { boxid: decodeURIComponent(req.query.uuid) };

    //Check box exists
    collection.findOne(query, function (err, result) {
      if (err) return res.status(500).json({ error: err });
      if (result) {
        //Look for all items assigned to box
        collection = client.db("home-inventory-system").collection("items");
        query = { "box.boxid": result.boxid };

        collection.find(query).toArray(function (err, results) {
          if (err) return res.status(500).json({ error: err });
          //Close connection and return results as array
          client.close();
          res.json({ results: results });
        });
      }
    });
  });
});

app.post("/api/additem", async (req, res, next) => {
  //Connect to database
  MongoClient.connect(process.env.mongourl, function (err, client) {
    if (err) res.status(500).json({ error: err });
    //Select collection
    let collection = client.db("home-inventory-system").collection("boxes");
    client.close();
    res.json({ error: "not all good" });
  });
});
