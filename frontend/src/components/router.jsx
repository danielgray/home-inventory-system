import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Homepage from "./home"
import Search from "./search"
import Additem from "./add"

export default function router() {
     return (
       <Router>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Home-Inventory</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <Link class="nav-link" to="/">Home</Link>
            </li>
            <li class="nav-item">
              <Link class="nav-link" to="/search">Search for an item</Link>
            </li>
            <li class="nav-item">
              <Link class="nav-link" to="/add">Add an item</Link>
            </li>
          </ul>
        </div>
      </nav>

      <Switch>
            <Route path="/search">
              <Search />
            </Route>
            <Route path="/add">
              <Additem />
            </Route>
            <Route path="/">
              <Homepage />
            </Route>
          </Switch>
      </Router>
     );
}

export class JustNav extends Component {
  render() {
    return (
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Home-Inventory</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
            <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/search">Search for an item</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/add">Add an item</a>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}