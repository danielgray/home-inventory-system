import React, { Component } from 'react'
const QRCode = require('qrcode.react')

export default class additem extends Component {
  state = {
    additem: true,
    response: {error: ""}
  }
  
  addNewItem = (query, cb) => {
    fetch(`/api/additem?${query}`, {
        method: "post",
        mode: "cors"
      })
        .then(response => response.json())
        .then((response) => {
          //Return error or nothing depending on response
          if(response.error) {
            console.log("Iran")
            cb(response.error)
          } else {
            cb(null)
          }
        })
        .catch(error => console.warn(error));
    }

    render() {
     return (
     <div class="container">
       <h1>Add a new item:</h1>

       <button onClick={() => this.setState({additem: !this.state.additem})} class="btn btn-info">Switch to adding a {this.state.additem? 'box': 'item'}</button>
       {this.state.additem? <SingleItem addNewItem={this.addNewItem}></SingleItem> : <Box></Box>}
     </div>
     )
    }
}

export class SingleItem extends Component {
  state = {
    itemName: '', 
    itemLocation: '',
    result: ''
  }

    //Update state with item details
    onFormChange = (e) => {
      this.setState({[e.target.name]: e.target.value})
    } 
    
    //Handle response from server
    testingFunc = (error) => {
      if(error) {
        this.setState({result: error})
      } else {
        this.setState({result: `'${this.state.itemName}' has been added to the inventory`})
      }
    }

  submitForm = (e) => {
    e.preventDefault()
    console.log(this.state)
    //Show error if fields left blank
    if(this.state.itemName === '' || this.state.itemLocation === '') {
      this.setState({response: {error: "Fields cannot be blank"}})
    } else {
      //Build query
      let query = `itemname=${this.state.itemName}&itemlocation=${this.state.itemLocation}`
      //send off to the server
      this.props.addNewItem(query, this.getResponse)
    }
  }

 render() {
   return (
    <div>
      <p>{this.state.result}</p>
      <form onSubmit={this.submitForm}class="form-group">
        <input onChange={this.onFormChange} type="text" name="itemName" placeholder="Item name"></input>
        <br></br>
        <input onChange={this.onFormChange} type="text" name="itemLocation" placeholder="Item Location"></input>
        <br></br>
        <button type="submit" class="btn btn-success">Add item</button>
      </form>
    </div>
   )
 }
}

export class Box extends Component {
  state = {
    result: ''
  }

  render() {
    return (
      <div>
        box
      </div>
    )
  }
}