import React, { Component } from 'react'
import QrReader from 'react-qr-reader'

export default class search extends Component {
    state = {
      searchtype: 'text',
      itemtofind: '',
      results: {error: "No items found"}
    }

    onFormChange = (e) => {
      this.setState({[e.target.name]: e.target.value})
    } 

    searchForItem = (e) => {
      e.preventDefault();
      fetch(`/api/search?itemtofind=${this.state.itemtofind.replace(/ /g, "+")}`, {
          method: "get",
          mode: "cors"
        })
          .then(response => response.json())
          //set state so we can render results
          .then((response) => {
            //Set state according to response
            if(response.error) {
              this.setState({results: response});
            } else {
              this.setState({results: response.results});
            }
          })
          .catch(error => console.warn(error));
    }

    render() {
        if (this.state.searchtype === "text") {
          //Search by text
            return (
                <div class="container">
                    <h1>Search for an item</h1>
                    <button onClick={() => this.setState({searchtype: 'qrcode'})} type="button" class="btn btn-secondary">Search by QR Code</button>
                    <form onSubmit={this.searchForItem} class="form-group">
                      <input onChange={this.onFormChange} name="itemtofind" type="text" placeholder="item"></input>
                      <br></br>
                      <button class="btn btn-success">Search</button>
                    </form>
                    <br></br>
                    {//Render error or results
                    !this.state.results.error? <Results results={this.state.results} query={this.state.itemtofind}></Results> : <p>{this.state.results.error}</p>}
                </div>
            )
        } else if (this.state.searchtype === "qrcode") {
          //Search via QR codes
            return (
                <div class="container">
                    <button onClick={() => this.setState({searchtype: 'text'})} type="button" class="btn btn-secondary">Search by text</button> 
                    <SearchQR></SearchQR>
                    <br></br>
                    {//Render error or results
                    !this.state.results.error? <Contents results={this.state.results}></Contents> : <p>{this.state.results.error}</p>}
                </div>
            )
        }
    }
}

export class SearchQR extends Component {
    state = {
        results: {error: "No items found"},
        showScanner: true
      }
    
      handleScan = data => {
        if (data) {
          fetch(`/api/getcontents?uuid=${data}`, {
          method: "get",
          mode: "cors"
        })
          .then(response => response.json())
          .then((response) => {
            //Set state according to response
            if(response.error) {
              this.setState({results: response});
            } else {
              this.setState({results: response.results, showScanner: false});
            }
          })
          .catch(error => console.warn(error));
        }
      }
      handleError = err => {
        console.error(err)
      }
      render() {
        return (
          <div>
            <button onClick={() => this.setState({showScanner: !this.state.showScanner})} type="button" class="btn btn-secondary">Toggle QR Scanner</button> 
            {this.state.showScanner? <div><p>Scan the QR code to see the contents of the box:</p>
            <QrReader
              delay={300}
              onError={this.handleError}
              onScan={this.handleScan}
              style={{ width: '100%' }}
            /> </div>: ''}
            {!this.state.results.error? <Contents results={this.state.results}></Contents> : ''}
          </div>
        )
      }
}

const Results = props => (
  <div>
    <h2>{props.results.length} results for <b>{props.query}</b>:</h2>
    <ul>
      { 
          props.results.map((result, index) => {
              return (
                <li>Location: {result.location} <br></br>
                {result.box? <p>Box: {result.box}</p> : ''}</li>
              )}
          )
      }
    </ul>
  </div>
)

const Contents = props => (
  <div>
    <h2>Contents:</h2>
    <ul>
      { 
          props.results.map((result, index) => {
              return (
                <li>{result.item}</li>
              )}
          )
      }
    </ul>
  </div>
)
