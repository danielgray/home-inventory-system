import React, { Component } from "react";
import "./App.css";
import Router, { JustNav } from "./components/router";

export default class app extends Component {
  state = {
    isLoading: true,
  };

  componentDidMount() {
    //Show loading wheel whilst loading
    let loader = document.getElementById("loader");
    setTimeout(() => this.setState({ isLoading: false }), 1000);
    loader.className += " fadeout";
  }

  render() {
    if (this.state.isLoading) {
      return [<JustNav></JustNav>, <Loader></Loader>];
    }
    return (
      //Mount the app with the router
      <div className="app">
        <Router></Router>
      </div>
    );
  }
}

export class Loader extends Component {
  render() {
    return (
      <div id="loader" class="text-center">
        <div class="spinner-border text-center loader" role="status">
          <span class="sr-only">Loading...</span>
        </div>
      </div>
    );
  }
}
